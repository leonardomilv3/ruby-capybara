require 'gem2deb/rake/spectask'

ENV.delete('CI')
ENV.delete('WEBDRIVERS')

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
end
